package turbovenik.messages.init;

public class Track {
    public String id;
    public String name;
    public Piece[] pieces;
    public Lane[] lanes;
    public Point startingPoint;
}
