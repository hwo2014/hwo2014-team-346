package turbovenik.messages.init;

import com.google.gson.annotations.SerializedName;

public class Piece {
    public double length;
    @SerializedName("switch")
    public boolean isSwitch;
    public double radius;
    public double angle;
    public boolean isStraight() {
        return length > 0;
    }
}
