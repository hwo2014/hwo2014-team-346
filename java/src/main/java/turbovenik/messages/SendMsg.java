package turbovenik.messages;

import com.google.gson.Gson;

public abstract class SendMsg {

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}
