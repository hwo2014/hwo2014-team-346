package turbovenik.messages;

public class Switch extends SendMsg {
    private String mydata;

    public Switch(String data) {
        this.mydata = data;
    }

    @Override
    protected Object msgData() {
        return mydata;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
