package turbovenik.messages;

/**
 * Created with IntelliJ IDEA.
 * User: pavel
 * Date: 17/04/14
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */
public class JoinRace extends SendMsg {

    public BotId botId;
    public String trackName;
    public String password;
    public int carCount;

    public JoinRace(BotId botId, String trackname, String password, int carCount) {
        this.botId = botId;
        this.trackName = trackname;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}
