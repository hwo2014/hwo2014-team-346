package turbovenik.messages;

/**
 * Created with IntelliJ IDEA.
 * User: pavel
 * Date: 17/04/14
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */
public class Join extends SendMsg {
    public String name;
    public String key;
    public int carCount = 3;

    public Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}
