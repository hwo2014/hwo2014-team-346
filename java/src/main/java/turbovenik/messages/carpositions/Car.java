package turbovenik.messages.carpositions;

/**
 * @author: pashka
 */
public class Car {
    public CarId id;
    public double angle;
    public PiecePosition piecePosition;
}
