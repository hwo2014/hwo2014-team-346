package turbovenik.messages.carpositions;

/**
 * @author: pashka
 */
public class PiecePosition {
    public int pieceIndex;
    public double inPieceDistance;
    public LaneInfo lane;
}
