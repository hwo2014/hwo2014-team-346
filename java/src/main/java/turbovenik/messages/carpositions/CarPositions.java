package turbovenik.messages.carpositions;

import com.google.gson.annotations.SerializedName;

/**
 * @author: pashka
 */
public class CarPositions {
    @SerializedName("data")
    public Car[] cars;
}
