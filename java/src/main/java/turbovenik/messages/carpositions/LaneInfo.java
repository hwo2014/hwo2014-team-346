package turbovenik.messages.carpositions;

/**
 * @author: pashka
 */
public class LaneInfo {
    public int startLaneIndex;
    public int endLaneIndex;
}
