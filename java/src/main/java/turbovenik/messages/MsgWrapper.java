package turbovenik.messages;

/**
 * Created with IntelliJ IDEA.
 * User: pavel
 * Date: 17/04/14
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */
public class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}
