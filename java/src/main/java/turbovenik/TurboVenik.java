package turbovenik;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;

import com.google.gson.Gson;
import turbovenik.messages.*;
import turbovenik.messages.carpositions.Car;
import turbovenik.messages.carpositions.CarPositions;
import turbovenik.messages.init.Data;
import turbovenik.messages.init.Lane;
import turbovenik.messages.init.Piece;
import turbovenik.messages.init.Track;

public class TurboVenik implements Runnable {

    public static final int BREAK_DIST = 40;
    public static final double MAX_V = 0.065;
    public final String host;
    public final int port;
    public final String botKey;

    private final String botName;

    public TurboVenik(String botName, String botKey, String host, int port) {
        this.botName = botName;
        this.botKey = botKey;
        this.host = host;
        this.port = port;
    }

    @Override
    public void run() {

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket;
        try {
            socket = new Socket(host, port);
            final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
            final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
            this.writer = writer;
            String line = null;
            send(new Join(botName, botKey));
//            send(new JoinRace(new BotId(botName, botKey), trackName, "", carNum));
            int c = 0;
            double prevDist = 0;
            double vv = 0;
            Piece lastSwitch = null;
            double maxmaxV = 0;
            int[][] needToSwitch = null;
            while ((line = reader.readLine()) != null) {
                final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
                if (msgFromServer.msgType.equals("carPositions")) {
                    CarPositions positions = gson.fromJson(line, CarPositions.class);
                    Car myCar = positions.cars[0];
                    for (Car car : positions.cars) {
                        if (car.id.name.equals(botName)) {
                            myCar = car;
                        }
                    }
                    int pieceIndex = myCar.piecePosition.pieceIndex;
                    Piece piece = track.pieces[pieceIndex];
                    int nextIndex = (pieceIndex + 1) % track.pieces.length;
                    Piece nextPiece = track.pieces[nextIndex];
                    double distToBend = piece.length - myCar.piecePosition.inPieceDistance;
                    int bendIndex = nextIndex;
                    while (track.pieces[bendIndex].isStraight()) {
                        distToBend += track.pieces[bendIndex].length;
                        bendIndex = (bendIndex + 1) % track.pieces.length;
                    }
                    Piece bendPiece = track.pieces[bendIndex];
                    int myLine = myCar.piecePosition.lane.endLaneIndex;
                    double r = getRaduis(bendPiece, track.lanes[myLine]);
                    if (nextPiece.isSwitch && nextPiece != lastSwitch) {
                        if (needToSwitch[myLine][nextIndex] == -1) {
                            System.out.print("L");
                            send(new Switch("Left"));
                        } else if (needToSwitch[myLine][nextIndex] == 1) {
                            System.out.print("R");
                            send(new Switch("Right"));
                        }
                        lastSwitch = nextPiece;
                    }
//                System.out.println(r);
                    double maxV = calcMaxV(r);
                    double v = prevDist - distToBend;
                    prevDist = distToBend;
                    if (v < 0) {
                        v = vv;
                    } else {
                        vv = v;
                    }
                    if (v > maxmaxV) maxmaxV = v;
                    if (distToBend > breakDist(v, maxV) || v < maxV * 9) {
                        send(new Throttle(1));
                        System.out.print("A");
                    } else if (v > maxV * 10) {
                        send(new Throttle(0));
                        System.out.print("!");
                    } else {
                        send(new Throttle(maxV));
                        System.out.print(".");
                    }
                    c++;
                    if (c % 50 == 0) {
                        System.out.println(" " + v);
                    }
                } else if (msgFromServer.msgType.equals("join")) {
                    System.out.println("Joined");
                } else if (msgFromServer.msgType.equals("gameInit")) {
                    System.out.println("Race init");
                    System.out.println(line);
                    Data data = gson.fromJson(msgFromServer.data.toString(), Data.class);
                    track = data.race.track;
                    double dist[][] = new double[track.lanes.length][track.pieces.length + 1];
                    needToSwitch = new int[track.lanes.length][track.pieces.length];
                    for (int i = track.pieces.length - 1; i >= 0; i--) {
                        for (int j = 0; j < track.lanes.length; j++) {
                            if (track.pieces[i].isStraight()) {
                                dist[j][i] = dist[j][i + 1];
                            } else {
                                double r = getRaduis(track.pieces[i], track.lanes[j]);
                                double v = calcMaxV(r);
                                double l = r * Math.abs(track.pieces[i].angle * Math.PI / 180);
                                dist[j][i] = dist[j][i + 1] + l / v;
                            }
                        }
                        if (track.pieces[i].isSwitch) {
                            for (int j = track.lanes.length - 1; j > 0; j--) {
                                if (dist[j - 1][i] < dist[j][i]) {
                                    needToSwitch[j][i] = -1;
                                    dist[j][i] = dist[j - 1][i];
                                }
                            }
                            for (int j = 0; j < track.lanes.length - 1; j++) {
                                if (dist[j + 1][i] < dist[j][i]) {
                                    needToSwitch[j][i] = 1;
                                    dist[j][i] = dist[j + 1][i];
                                }
                            }
                        }
                    }
                    System.out.println(Arrays.toString(needToSwitch[0]));
                    System.out.println(Arrays.toString(needToSwitch[1]));
                } else if (msgFromServer.msgType.equals("gameEnd")) {
                    System.out.println("Race end");
                    send(new Ping());
                } else if (msgFromServer.msgType.equals("gameStart")) {
                    System.out.println("Race start");
                    send(new Ping());
                } else if (msgFromServer.msgType.equals("dnf")) {
                    System.out.println("DNF: " + line);
                    send(new Ping());
                } else {
//                    System.out.println(line);
                    send(new Ping());
                }
            }
            System.out.println(maxmaxV);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private double breakDist(double vCur, double vNeed) {
        if (vNeed >= vCur) return 0;
        return Math.log(vCur / vNeed) * BREAK_DIST;
    }

    private double getRaduis(Piece piece, Lane lane) {
        if (piece.angle < 0) {
            return piece.radius + lane.distanceFromCenter;
        } else {
            return piece.radius - lane.distanceFromCenter;
        }
    }

    private double calcMaxV(double r) {
        if (r < 50) {
            return MAX_V * Math.sqrt(r);
        } else {
            return MAX_V * Math.sqrt(r);
        }
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    Track track;

    private void send(final SendMsg msg) {
//        System.out.println(gson.toJson(new MsgWrapper(msg)));
        writer.println(gson.toJson(new MsgWrapper(msg)));
        writer.flush();
    }

}

