package turbovenik;

/**
 * @author: pashka
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
//          new TurboVenik("Venik", "keimola", 1).run();
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        new TurboVenik(botName, botKey, host, port).run();
    }
}
